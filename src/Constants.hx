/**
	Constants are constants value / magic numbers for the game.
	These should be set to be final.

	For the evil globals counterpart, see Globals.hx
	For the function counterpart, see Utils.hx
**/
class Constants {
	public static final Version: String = "0.1.2";
	public static final GitBuild: String = '${zf.Build.getGitCommitHash()}';

	public static final ButtonSize: Point2i = [70, 20];
}
