/**
	Utils store all the functions that generally useful for the whole repo, but not yet lib/common level.

	For the globals variable, see Globals.hx
	For constants, see Constants.hx

**/
class Utils {
	public static function animatePop(animator: zf.animations.Animator, object: h2d.Object) {
		final wo = object.wo();
		wo.alignCenter();
		animator.runAnim(new Pop(wo, .3, .3), () -> {
			wo.returnCenter();
		});
	}
}
