import zf.ui.GenericButton;

class URLButton extends zf.ui.Button {
	var obj: h2d.Object;

	public var floatUpAmt: Float = -2;

	public var url: String;

	public function new(id: String, labelString: String, url: String) {
		var icon = Globals.res.getBitmap('button:${id}');
		final bound = icon.getSize();
		super(Std.int(bound.width), Std.int(bound.height));
		this.url = url;

		this.obj = new h2d.Object();
		this.addChild(obj);
		obj.addChild(icon);

		final label = new h2d.Text(Assets.bodyFont2x);
		label.text = labelString;
		obj.addChild(label);
		label.putOnRight(icon, [4, 0]).centerYWithin(icon);
	}

	override function onLeftClick() {
		hxd.System.openURL(this.url);
	}

	override function updateButton() {
		if (this.isOver) {
			this.obj.y = this.floatUpAmt;
		} else {
			this.obj.y = 0;
		}
	}
}

class MenuScreen extends zf.Screen {
	public var title: h2d.Object;

	public var startButton: GenericButton;
	public var discordButton: zf.ui.Button;
	public var dicetribeButton: zf.ui.Button;

	public function new() {
		super();

		this.title = Globals.res.getBitmap("title");
		this.title.setX(Globals.game.gameWidth, AlignCenter).setY(30);
		this.addChild(title);

		inline function makeButton(title: String): GenericButton {
			final button = new GenericButton(Assets.fromColor(Colors.ButtonBasic[0], Constants.ButtonSize.x + 20,
				Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[1], Constants.ButtonSize.x + 20, Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[2], Constants.ButtonSize.x + 20, Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[3], Constants.ButtonSize.x + 20, Constants.ButtonSize.y));
			button.font = Assets.bodyFont3x;
			button.text = title;
			return button;
		}

		this.startButton = makeButton("NEW GAME");
		this.startButton.setX(Globals.game.gameWidth, AlignCenter).setY(150);
		this.startButton.onLeftClick = function() {
			this.game.switchScreen(new GameScreen());
		}
		this.addChild(this.startButton);

		this.discordButton = new URLButton("discord", "JOIN DISCORD", "https://discord.gg/BsNrBVf8ev");
		this.discordButton.setX(30).setY(Globals.game.gameHeight, AnchorBottom, 30);
		this.addChild(discordButton);

		this.dicetribeButton = new URLButton("dicetribe", "CHECK OUT DICE TRIBE: AMBITIONS", "https://zwodahs.itch.io/dice-tribes-ambitions");
		this.dicetribeButton.putOnRight(this.discordButton, [10, 0]).setY(Globals.game.gameHeight, AnchorBottom, 30);
		this.addChild(dicetribeButton);
	}
	var elapsed: Float = 0;

	override public function update(dt: Float) {
		elapsed += dt;
		this.title.y = 30 + (Math.sin(elapsed) * 10);
	}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {}

	override public function destroy() {}
}
