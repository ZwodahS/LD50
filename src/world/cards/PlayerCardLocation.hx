package world.cards;

enum PlayerCardLocation {
	Void;
	Deck;
	Hand;
	Playing; // currently activated card
	Reward;
}
