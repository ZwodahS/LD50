package world.cards;

class PainEffect extends CardEffect {
	public var amount: Int;

	public function new(amount: Int) {
		super(Heal);
		this.amount = amount;
	}

	override public function getRenderObject(): zf.tui.TemplateConf {
		final string: String = 'PAIN ${this.amount}';
		return {
			type: "htmltext",
			conf: {
				text: string,
				font: Assets.bodyFont2x,
				textColor: Colors.Black[1],
				textAlign: "center",
				maxWidth: 80,
			}
		}
	}

	override public function applyEffect(world: World) {
		final enemy = world.battleSystem.currentEnemy;
		if (enemy == null) return;
		world.damagePlayer(this.amount);
	}

	override public function getKeyword(): String {
		return "PAIN";
	}

	override public function getKeywordTooltip(): String {
		return 'Take ${this.amount} damage.';
	}
}
