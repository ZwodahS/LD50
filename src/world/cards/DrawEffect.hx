package world.cards;

class DrawEffect extends CardEffect {
	public var amount: Int;

	public function new(amount: Int) {
		super(Discard);
		this.amount = amount;
	}

	override public function getRenderObject(): zf.tui.TemplateConf {
		final string: String = 'DRAW ${this.amount}';
		return {
			type: "htmltext",
			conf: {
				text: string,
				font: Assets.bodyFont2x,
				textColor: Colors.Black[1],
				textAlign: "center",
				maxWidth: 80,
			}
		}
	}

	override public function applyEffect(world: World) {
		final enemy = world.battleSystem.currentEnemy;
		if (enemy == null) return;
		world.drawCards(this.amount);
	}

	override public function getKeyword(): String {
		return "DRAW";
	}

	override public function getKeywordTooltip(): String {
		return 'Draw ${this.amount} cards.';
	}
}
