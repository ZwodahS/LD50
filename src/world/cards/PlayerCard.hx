package world.cards;

class PlayerCard extends Card {
	public var location: PlayerCardLocation = Void;

	public var cardName: String;
	public var playerCardType(default, null): PlayerCardType;

	public var currentDurability: Int;
	public var maxDurability: Int;

	public var effects: Array<CardEffect>;

	public var durabilityIcons: Array<DurabilityIcon>;

	public var targetPosition: Point2f;

	public function new(name: String, cardType: PlayerCardType, maxDurability: Int, effects: Array<CardEffect>) {
		super();
		this.cardName = name;
		this.playerCardType = cardType;
		this.maxDurability = maxDurability;
		this.currentDurability = maxDurability;
		this.effects = effects;
		for (effect in effects) {
			effect.card = this;
		}
		render();
	}

	function render() {
		setCardBackgound(Globals.res.getBitmap("card:bg", this.playerCardType));

		final bounds = getBounds();

		var text = new h2d.Text(this.cardName.length > 10 ? Assets.bodyFont1x : Assets.bodyFont2x);
		text.text = this.cardName;
		text.x = 10;
		text.setY(26, AlignCenter);
		text.textColor = Colors.Black[1];
		this.addChild(text);

		final items: Array<zf.tui.TemplateConf> = [];
		for (effect in this.effects) {
			final o = effect.getRenderObject();
			if (o == null) continue;
			items.push(o);
		}
		final flow = Globals.tui.createObject({
			type: "flow:vertical",
			conf: {
				items: items,
				spacing: 3,
			}
		});
		this.addChild(flow);
		flow.setY(32);

		final flow = Globals.tui.createObject({
			type: "flow:horizontal",
			conf: {items: [], spacing: 2}
		});
		this.durabilityIcons = [];
		for (_ in 0...this.maxDurability) {
			final icon = new DurabilityIcon();
			icon.depleted = false;
			this.durabilityIcons.push(icon);
			flow.addChild(icon);
		}

		this.addChild(flow);
		flow.setY(bounds.height, AnchorBottom, 8).setX(8);
	}

	public function update(dt: Float) {
		if (this.targetPosition != null) {
			var moveVec: Point2f = this.targetPosition - [this.x, this.y];
			var moveAmt = moveVec.unit * dt * 800;
			if (this.targetPosition.x > this.x) {
				this.x += moveAmt.x;
				if (this.x > this.targetPosition.x) this.x = this.targetPosition.x;
			} else if (this.targetPosition.x < this.x) {
				this.x += moveAmt.x;
				if (this.x < this.targetPosition.x) this.x = this.targetPosition.x;
			}

			if (this.targetPosition.y > this.y) {
				this.y += moveAmt.y;
				if (this.y > this.targetPosition.y) this.y = this.targetPosition.y;
			} else if (this.targetPosition.y < this.y) {
				this.y += moveAmt.y;
				if (this.y < this.targetPosition.y) this.y = this.targetPosition.y;
			}
			if (this.x == this.targetPosition.x && this.y == this.targetPosition.y) {
				this.targetPosition = null;
			}
		}
	}

	public function depleteDurability(animator: Animator) {
		this.currentDurability -= 1;
		this.durabilityIcons[this.currentDurability].animateDeplete(animator);
	}

	public function gainDurability(animator: Animator) {
		if (this.currentDurability >= this.maxDurability) return;
		this.durabilityIcons[this.currentDurability].animateGain(animator);
		this.currentDurability += 1;
	}
}
