package world.cards;

class WoundEffect extends CardEffect {
	public var amount: Int;

	public function new(amount: Int) {
		super(Discard);
		this.amount = amount;
	}

	override public function getRenderObject(): zf.tui.TemplateConf {
		final string: String = 'Wound ${this.amount}';
		return {
			type: "htmltext",
			conf: {
				text: string,
				font: Assets.bodyFont2x,
				textColor: Colors.Black[1],
				textAlign: "center",
				maxWidth: 80,
			}
		}
	}

	override public function getKeyword(): String {
		return "Wound";
	}

	override public function getKeywordTooltip(): String {
		return 'Add ${this.amount} Wound card to Deck.';
	}

	override public function applyEffect(world: World) {
		final enemy = world.battleSystem.currentEnemy;
		if (enemy == null) return;
		world.moveCard(new PlayerCard("Wound", Physical, 1, [new PainEffect(2)]), Deck);
	}
}
