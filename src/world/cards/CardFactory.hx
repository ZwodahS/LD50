package world.cards;

class CardFactory {
	public var table: zf.ProbabilityTable<Void->PlayerCard>;
	public var map: Map<String, Void->PlayerCard>;

	public function new() {
		this.table = new zf.ProbabilityTable<Void->PlayerCard>();
		this.map = new Map<String, Void->PlayerCard>();

		inline function addCard(id: String, weight: Int, func: Void->PlayerCard) {
			this.table.add(weight, func);
			this.map[id] = func;
		}

		addCard('Stab', 100, () -> {
			return new PlayerCard("Stab", Physical, 4, [new DamageEffect(3)]);
		});
		addCard('Slam', 100, () -> {
			return new PlayerCard("Slam", Physical, 3, [new DamageEffect(5)]);
		});
		addCard('Heavy Slam', 100, () -> {
			return new PlayerCard("Heavy Slam", Physical, 3, [new DamageEffect(8), new PainEffect(1)]);
		});
		addCard('Sacrifice', 50, () -> {
			return new PlayerCard("Sacrifice", Physical, 2, [new DamageEffect(12), new DestroyEffect(1)]);
		});
		addCard('Rage', 50, () -> {
			return new PlayerCard('Rage', Physical, 3, [new DamageEffect(7), new WoundEffect(1)]);
		});

		addCard("Resurrection", 25, () -> {
			return new PlayerCard("Resurrection", Magical, 1, [new HealEffect(15), new DestroyEffect(1)]);
		});
		addCard("IceBolt", 75, () -> {
			return new PlayerCard("Ice Bolt", Magical, 3,
				[new DamageEffect(4), new ShuffleEffect(1), new DrawEffect(1)]);
		});
		addCard("Blizzard", 25, () -> {
			return new PlayerCard("Blizzard", Magical, 3,
				[new DamageEffect(9), new ShuffleEffect(2), new DrawEffect(2)]);
		});
		addCard("FireBlast", 25, () -> {
			return new PlayerCard("FireBlast", Magical, 2, [new DamageEffect(15), new PainEffect(3)]);
		});
		addCard("Fireball", 50, () -> {
			return new PlayerCard("Fireball", Magical, 2, [new DamageEffect(10), new PainEffect(2)]);
		});
		addCard("Recovery", 25, () -> {
			return new PlayerCard("Recovery", Magical, 2, [new HealEffect(5), new ShuffleEffect(5)]);
		});
		addCard("Leech Life", 25, () -> {
			return new PlayerCard("Leech Life", Magical, 3, [new DamageEffect(4), new HealEffect(2)]);
		});

		addCard("Bandage", 50, () -> {
			return new PlayerCard("Bandage", Item, 4, [new HealEffect(5)]);
		});
		addCard("Healing Soup", 25, () -> {
			return new PlayerCard("Healing Soup", Item, 2,
				[new HealEffect(2), new ShuffleEffect(1), new DrawEffect(1)]);
		});

		addCard("Insight", 50, () -> {
			return new PlayerCard("Insight", Tactical, 3, [new DrawEffect(2)]);
		});

		addCard("Recycle", 50, () -> {
			return new PlayerCard("Recycle", Tactical, 2, [new ShuffleEffect(3), new DrawEffect(3)]);
		});
	}

	public function randomCard(world: World) {
		final card = this.table.randomItem(world.r, false);
		return card();
	}

	public function getStartingCards(world: World): Array<PlayerCard> {
		final cards = [];
		inline function addCard(id: String, count: Int) {
			final c = this.map[id];
			for (_ in 0...count) {
				cards.push(c());
			}
		}
		addCard("Stab", 3);
		addCard("Slam", 2);
		addCard("Heavy Slam", 2);
		addCard("Sacrifice", 1);
		addCard("Fireball", 1);
		addCard("IceBolt", 1);
		addCard("Bandage", 2);
		addCard("Insight", 2);
		addCard("Recycle", 1);
		for (_ in 0...5) {
			cards.push(randomCard(world));
		}
		return cards;
	}

	public function makeCard(id: String): PlayerCard {
		return this.map[id]();
	}
}
