package world.cards;

class ShuffleEffect extends CardEffect {
	public var amount: Int;

	public function new(amount: Int) {
		super(Discard);
		this.amount = amount;
	}

	override public function getRenderObject(): zf.tui.TemplateConf {
		final string: String = 'SHUFFLE ${this.amount}';
		return {
			type: "htmltext",
			conf: {
				text: string,
				font: Assets.bodyFont2x,
				textColor: Colors.Black[1],
				textAlign: "center",
				maxWidth: 80,
			}
		}
	}

	override public function applyEffect(world: World) {
		final enemy = world.battleSystem.currentEnemy;
		if (enemy == null) return;
		final cards: Array<PlayerCard> = world.r.randomChoices(world.state.inHand, this.amount);
		for (card in cards) {
			world.moveCard(card, Deck);
		}
	}

	override public function getKeyword(): String {
		return "SHUFFLE";
	}

	override public function getKeywordTooltip(): String {
		return 'Shuffle ${this.amount} cards from hand to deck.';
	}
}
