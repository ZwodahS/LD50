package world.cards;

class HealEffect extends CardEffect {
	public var healAmt: Int;

	public function new(healAmt: Int) {
		super(Heal);
		this.healAmt = healAmt;
	}

	override public function getRenderObject(): zf.tui.TemplateConf {
		final string: String = 'HEAL ${this.healAmt}';
		return {
			type: "htmltext",
			conf: {
				text: string,
				font: Assets.bodyFont2x,
				textColor: Colors.Black[1],
				textAlign: "center",
				maxWidth: 80,
			}
		}
	}

	override public function applyEffect(world: World) {
		final enemy = world.battleSystem.currentEnemy;
		if (enemy == null) return;
		world.gainPlayerHealth(this.healAmt);
	}

	override public function getKeyword(): String {
		return "HEAL";
	}

	override public function getKeywordTooltip(): String {
		return 'Heal for ${this.healAmt}.';
	}
}
