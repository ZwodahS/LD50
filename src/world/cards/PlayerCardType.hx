package world.cards;

enum abstract PlayerCardType(Int) from Int to Int {
	public var Item = 0;
	public var Physical = 1;
	public var Magical = 2;
	public var Tactical = 3;
}
