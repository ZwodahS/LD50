package world.cards;

enum CardEffectType {
	Damage;
	Armor;
	Heal;
	Draw;
	Discard;
	Stun;
	Run;
}
