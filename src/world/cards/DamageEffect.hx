package world.cards;

class DamageEffect extends CardEffect {
	public var damagePerHit: Int;
	public var numHit: Int;

	public function new(damagePerHit: Int, numHit: Int = 1) {
		super(Damage);
		this.damagePerHit = damagePerHit;
		this.numHit = numHit;
	}

	override public function getRenderObject(): zf.tui.TemplateConf {
		var string: String = '';
		if (numHit == 1) {
			string = 'DAMAGE ${damagePerHit}';
		} else {
			string = 'DAMAGE ${damagePerHit} x ${numHit}';
		}
		return {
			type: "htmltext",
			conf: {
				text: string,
				font: Assets.bodyFont2x,
				textColor: Colors.Black[1],
				textAlign: "center",
				maxWidth: 80,
			}
		}
	}

	override public function getKeyword(): String {
		return "DAMAGE";
	}

	override public function getKeywordTooltip(): String {
		return 'Deals ${damagePerHit} to enemy';
	}

	override public function applyEffect(world: World) {
		final enemy = world.battleSystem.currentEnemy;
		if (enemy == null) return;
		world.battleSystem.damageEnemy(this.card, this.damagePerHit, this.numHit);
	}
}
