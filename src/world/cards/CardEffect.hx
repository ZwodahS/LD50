package world.cards;

class CardEffect {
	public var type: CardEffectType;
	public var card: PlayerCard;

	public function new(type: CardEffectType) {
		this.type = type;
	}

	public function getRenderObject(): zf.tui.TemplateConf {
		return null;
	}

	public function getKeyword(): String {
		return null;
	}

	public function getKeywordTooltip(): String {
		return null;
	}

	public function applyEffect(world: World) {}
}
