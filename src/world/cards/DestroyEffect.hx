package world.cards;

class DestroyEffect extends CardEffect {
	public var amount: Int;

	public function new(amount: Int) {
		super(Discard);
		this.amount = amount;
	}

	override public function getRenderObject(): zf.tui.TemplateConf {
		final string: String = 'DESTROY ${this.amount}';
		return {
			type: "htmltext",
			conf: {
				text: string,
				font: Assets.bodyFont2x,
				textColor: Colors.Black[1],
				textAlign: "center",
				maxWidth: 80,
			}
		}
	}

	override public function getKeyword(): String {
		return "DESTROY";
	}

	override public function getKeywordTooltip(): String {
		return 'Destroy ${this.amount} random card in Hand.';
	}

	override public function applyEffect(world: World) {
		final enemy = world.battleSystem.currentEnemy;
		if (enemy == null) return;
		final cards: Array<PlayerCard> = world.r.randomChoices(world.state.inHand, this.amount);
		for (card in cards) {
			if (card == null) continue;
			world.moveCard(card, Void);
		}
	}
}
