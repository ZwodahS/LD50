package world.events;

enum abstract EventType(Int) from Int to Int {
	public var BattleEvent = 1;
	public var RunEvent = 2;
}
