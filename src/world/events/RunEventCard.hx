package world.events;

class RunEventCard extends EventCard {
	public var requiredColors: Array<PlayerCardType>;

	public function new(colors: Array<PlayerCardType>) {
		super(RunEvent, "Run");
		this.requiredColors = colors;

		render();
	}

	function render() {
		final items: Array<zf.tui.TemplateConf> = [];
		for (i in this.requiredColors) {
			final o = Globals.res.getBitmap("icon:minicard", i);
			items.push({type: "object", conf: {object: o}});
		}
		final flow = Globals.tui.createObject({
			type: "flow:horizontal",
			conf: {
				items: items,
				spacing: 5,
			}
		});
		final bounds = this.getBounds();
		this.add(flow, 50);
		flow.setX(bounds.width, AlignCenter).setY(bounds.height, AnchorBottom, 15);
	}
}
