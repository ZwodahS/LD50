package world.events;

class EventCard extends Card {
	public var title: h2d.HtmlText;
	public var eventType: EventType;

	public function new(eventType: EventType, name: String) {
		super();
		this.eventType = eventType;
		setCardBackgound(Globals.res.getBitmap("card:event:bg", eventType));

		final bounds = this.getBounds();

		this.title = new h2d.HtmlText(Assets.bodyFont2x);
		this.title.x = 4;
		this.title.y = 20;
		this.title.text = name;
		this.title.textColor = Colors.Black[1];
		this.title.textAlign = Center;
		this.title.maxWidth = bounds.width - 8;
		this.add(this.title, 10);
	}
}
