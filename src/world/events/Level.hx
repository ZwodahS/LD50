package world.events;

enum LevelType {
	Battle;
	Rest;
}

class Level {
	public var enemy: EnemyCard;
	public var type: LevelType;
	public var rewardCards: Int = 0;
	public var rewardHealth: Int = 0;

	public function new(type: LevelType, enemy: EnemyCard = null) {
		this.type = type;
		this.enemy = enemy;
	}

	public function hasReward() {
		return this.rewardCards > 0 || this.rewardHealth > 0;
	}
}
