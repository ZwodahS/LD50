package world;

class EnemyFactory {
	public var map: Map<String, Void->EnemyCard>;

	public function new() {
		this.map = new Map<String, Void->EnemyCard>();

		inline function addCard(name: String, func: Void->EnemyCard) {
			this.map[name] = func;
		}

		addCard("Rat", () -> {
			var c = new EnemyCard("Rat", 5, 0, 2);
			return c;
		});

		addCard("Wolf", () -> {
			var c = new EnemyCard("Wolf", 25, 0, 3);
			c.rewardCards = 1;
			return c;
		});

		addCard("Tortoise", () -> {
			var c = new EnemyCard("Tortoise", 35, 0, 1);
			c.rewardCards = 2;
			return c;
		});

		addCard("Dragon", () -> {
			var c = new EnemyCard("Dragon", 60, 0, 5);
			c.rewardCards = 4;
			c.rewardHealth = 8;
			return c;
		});

		addCard("GreaterDragon", () -> {
			var c = new EnemyCard("Greater Dragon", 80, 0, 8);
			c.rewardCards = 5;
			c.rewardHealth = 10;
			return c;
		});

		addCard("Spider", () -> {
			var c = new EnemyCard("Spider", 20, 0, 4);
			c.rewardCards = 1;
			return c;
		});

		addCard("Bat", () -> {
			var c = new EnemyCard("Bat", 12, 0, 3);
			c.rewardHealth = 3;
			return c;
		});
	}

	public function makeEnemy(id: String) {
		return this.map[id]();
	}

	public function getEnemy(depth: Int, world: World) {
		var enemies: Array<String> = [];
		if (depth < 5) {
			enemies = ["Rat", "Bat", "Spider"];
		} else if (depth < 15) {
			enemies = ["Rat", "Bat", "Spider", "Wolf", "Tortoise"];
		} else if (depth < 25) {
			enemies = ["Spider", "Wolf", "Tortoise", "Dragon"];
		} else {
			enemies = ["Dragon", "Dragon", "GreaterDragon"];
		}
		var choice = enemies.randomItem(world.r);
		if (this.map.exists(choice) == false) {
			Logger.debug(choice);
			choice = "Dragon";
		}
		return this.map[choice]();
	}
}
