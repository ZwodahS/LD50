package world;

class Card extends h2d.Layers {
	public var bg: h2d.Bitmap;
	public var interactive: h2d.Interactive;

	public function new() {
		super();
	}

	function setCardBackgound(bg: h2d.Bitmap) {
		if (this.bg != null) this.bg.remove();
		this.add(this.bg = bg, 0);
		if (this.interactive != null) this.interactive.remove();
		final bounds = getBounds();

		this.add(this.interactive = new h2d.Interactive(bounds.width, bounds.height), 150);
		this.interactive.onOver = function(e: hxd.Event) {
			onOver(this, e);
		}
		this.interactive.onOut = function(e: hxd.Event) {
			onOut(this, e);
		}
		this.interactive.onClick = function(e: hxd.Event) {
			if (e.button == 1) {
				onRightClick(this, e);
			} else {
				onLeftClick(this, e);
			}
			onClick(this, e);
		}
		this.interactive.onKeyDown = function(e: hxd.Event) {
			onKeyDown(this, e);
		}
		this.interactive.onPush = function(e: hxd.Event) {
			onPush(this, e);
		}
		this.interactive.onRelease = function(e: hxd.Event) {
			onRelease(this, e);
		}
		this.interactive.onMove = function(e: hxd.Event) {
			onMove(this, e);
		}
		this.interactive.enableRightButton = true;
		this.interactive.cursor = Default;
		this.interactive.propagateEvents = false;
	}

	/**
		Always use the bounds of the bg
	**/
	override public function getBounds(?relativeTo: h2d.Object, ?out: h2d.col.Bounds): h2d.col.Bounds {
		return bg.getBounds(relativeTo, out);
	}

	dynamic public function onOver(card: Card, event: hxd.Event) {}

	dynamic public function onOut(card: Card, event: hxd.Event) {}

	dynamic public function onLeftClick(card: Card, event: hxd.Event) {}

	dynamic public function onRightClick(card: Card, event: hxd.Event) {}

	dynamic public function onClick(card: Card, event: hxd.Event) {}

	dynamic public function onKeyDown(card: Card, event: hxd.Event) {}

	dynamic public function onPush(card: Card, event: hxd.Event) {}

	dynamic public function onRelease(card: Card, event: hxd.Event) {}

	dynamic public function onMove(card: Card, event: hxd.Event) {}
}
