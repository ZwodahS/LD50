package world.messages;

class MStartFight extends zf.Message {
	public static final MessageType = "MStartFight";

	public var enemy: EnemyCard;

	public function new(enemy: EnemyCard) {
		super(MessageType);
		this.enemy = enemy;
	}
}
