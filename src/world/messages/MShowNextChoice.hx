package world.messages;

class MShowNextChoice extends zf.Message {
	public static final MessageType = "MShowNextChoice";

	public function new() {
		super(MessageType);
	}
}
