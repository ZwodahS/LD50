package world.messages;

class MSetTitle extends zf.Message {
	public static final MessageType = "MSetTitle";

	public var title: String;

	public function new(title: String) {
		super(MessageType);
		this.title = title;
	}
}
