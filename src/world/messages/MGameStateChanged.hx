package world.messages;

class MGameStateChanged extends zf.Message {
	public static final MessageType = "MGameStateChanged";

	public var oldValue: GameState;
	public var newValue: GameState;

	public function new(oldValue: GameState, newValue: GameState) {
		super(MessageType);
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
}
