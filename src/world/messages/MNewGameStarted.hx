package world.messages;

class MNewGameStarted extends zf.Message {
	public static final MessageType = "MNewGameStarted";

	public function new() {
		super(MessageType);
	}
}
