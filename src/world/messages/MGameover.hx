package world.messages;

class MGameover extends zf.Message {
	public static final MessageType = "MGameover";

	public var reason: String;

	public function new(reason: String) {
		super(MessageType);
		this.reason = reason;
	}
}
