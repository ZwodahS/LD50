package world.messages;

class MCardPlayed extends zf.Message {
	public static final MessageType = "MCardPlayed";

	public var card: PlayerCard;

	public function new(card: PlayerCard) {
		super(MessageType);
		this.card = card;
	}
}
