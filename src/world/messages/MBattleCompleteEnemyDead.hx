package world.messages;

class MBattleCompleteEnemyDead extends zf.Message {
	public static final MessageType = "MBattleCompleteEnemyDead";

	public var enemy: EnemyCard;

	public function new(enemy: EnemyCard) {
		super(MessageType);
		this.enemy = enemy;
	}
}
