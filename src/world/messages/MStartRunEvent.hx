package world.messages;

class MStartRunEvent extends zf.Message {
	public static final MessageType = "MStartRunEvent";

	public function new(card: RunEventCard) {
		super(MessageType);
	}
}
