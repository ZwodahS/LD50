package world.messages;

class MPlayerCardMoved extends zf.Message {
	public static final MessageType = "MPlayerCardMoved";

	public var card: PlayerCard;
	public var source: PlayerCardLocation;
	public var destination: PlayerCardLocation;

	public function new(card: PlayerCard, source: PlayerCardLocation, destination: PlayerCardLocation) {
		super(MessageType);
		this.card = card;
		this.source = source;
		this.destination = destination;
	}
}
