package world.messages;

class MPlayerHealthChanged extends zf.Message {
	public static final MessageType = "MPlayerHealthChanged";

	public function new() {
		super(MessageType);
	}
}
