package world;

class EnemyCard extends h2d.Object {
	public var bg: h2d.Bitmap;

	public var enemyName: String;

	public var currentHealth(default, set): Int;
	public var maxHealth: Int;
	public var armor: Int;
	public var strength: Int;

	public var healthIcon: HealthIcon;
	public var strengthIcon: StrengthIcon;

	public var rewardCards: Int = 0;
	public var rewardHealth: Int = 0;
	public var specificCards: Array<PlayerCard> = null;

	public function new(name: String, health: Int, armor: Int, strength: Int) {
		super();
		this.addChild(this.bg = Globals.res.getBitmap("card:enemy:bg", 2));
		this.enemyName = name;

		this.maxHealth = health;
		this.currentHealth = this.maxHealth;
		this.armor = armor;
		this.strength = strength;

		final bounds = this.bg.getBounds();
		var title = new h2d.HtmlText(this.enemyName.length <= 12 ? Assets.bodyFont2x : Assets.bodyFont1x);
		title.text = name.toUpperCase();
		title.maxWidth = bounds.width - 8;
		title.textAlign = Center;
		title.textColor = Colors.Black[1];
		title.x = 4;
		title.y = 20;
		this.addChild(title);

		this.healthIcon = new HealthIcon('${this.currentHealth}');
		this.addChild(this.healthIcon);
		this.healthIcon.x = 4;
		this.healthIcon.setY(bounds.height, AnchorBottom, 4);

		this.strengthIcon = new StrengthIcon('${this.strength}');
		this.addChild(this.strengthIcon);
		this.strengthIcon.setX(bounds.width, AnchorRight, 4);
		this.strengthIcon.setY(bounds.height, AnchorBottom, 4);
	}

	public function set_currentHealth(h: Int): Int {
		this.currentHealth = h;
		if (this.healthIcon != null) this.healthIcon.textValue = '${this.currentHealth}';
		return this.currentHealth;
	}
}
