package world;

class WorldState {
	public var currentHealth: Int;
	public var maxHealth: Int;
	public var inDeck: Array<PlayerCard>;
	public var inHand: Array<PlayerCard>;

	public var distance: Int = 0;

	public var currentChoices: Array<Card>;

	public var dragonSlayed: Int = 0;

	public function new() {
		this.currentHealth = 30;
		this.maxHealth = 30;
		this.inDeck = [];
		this.inHand = [];
		this.distance = 0;
		this.dragonSlayed = 0;
	}
}
