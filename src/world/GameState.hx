package world;

enum GameState {
	Intro;
	Upkeep; // drawing cards and player upkeep
	PlayerTurn; // player turn idle, waiting for player's action
	AnimatingCard; // animating card effects
	EnemyTurn; // animating enemies effect
	Cleanup; // after enemy turn cleanup
	ChoosingPath; // currently choosing the next path
	Animating;
	EndOfBattle;
}
