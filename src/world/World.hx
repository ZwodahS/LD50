package world;

/**
	Store the game state and handle game logic
**/
class World {
	public var gameState(default, set): GameState = Intro;

	public function set_gameState(s: GameState): GameState {
		final old = this.gameState;
		this.gameState = s;
		this.dispatcher.dispatch(new MGameStateChanged(old, s));
		return this.gameState;
	}

	public var state: WorldState;
	public var r: hxd.Rand;

	public var blockingAnimator: zf.animations.Animator;
	public var nonBlockingAnimator: zf.animations.Animator;

	public var systems: Array<System>;
	public var renderSystem: RenderSystem;
	public var choiceSystem: ChoiceSystem;
	public var battleSystem: BattleSystem;
	public var cardSystem: PlayerCardSystem;

	public var dispatcher: MessageDispatcher;

	public function new() {
		this.r = new hxd.Rand(Random.int(0, zf.Constants.SeedMax));
		this.dispatcher = new MessageDispatcher();
		this.blockingAnimator = new zf.animations.Animator();
		this.nonBlockingAnimator = new zf.animations.Animator();
		this.systems = [];

		addSystem(this.renderSystem = new RenderSystem());
		addSystem(this.choiceSystem = new ChoiceSystem());
		addSystem(this.battleSystem = new BattleSystem());
		addSystem(this.cardSystem = new PlayerCardSystem());
	}

	public function addSystem(system: System) {
		this.systems.push(system);
		system.init(this);
		return system;
	}

	public function startNewGame(tutorial: Bool = true) {
		for (s in this.systems) {
			s.reset();
		}
		if (tutorial) {
			this.dispatcher.dispatch(new MShowTutorial());
		} else {
			this.startRun();
		}
	}

	public function startRun() {
		this.state = new WorldState();

		this.gameState = Intro;
		this.dispatcher.dispatch(new MNewGameStarted());
		this.nonBlockingAnimator.waitFor(function() {
			return this.blockingAnimator.count == 0;
		}, function() {
			addStartingCards();
			this.nonBlockingAnimator.waitFor(function() {
				return this.blockingAnimator.count == 0;
			}, function() {
				drawCards();
				showNextChoice();
			});
		});
	}

	public function showNextChoice() {
		this.gameState = ChoosingPath;
		this.dispatcher.dispatch(new MShowNextChoice());
	}

	public function update(dt: Float) {
		this.blockingAnimator.update(dt);
		this.nonBlockingAnimator.update(dt);
		for (s in this.systems) {
			s.update(dt);
		}
	}

	public function damagePlayer(damage: Int) {
		this.state.currentHealth -= damage;
		this.state.currentHealth = Math.clampI(this.state.currentHealth, 0, null);
		this.dispatcher.dispatch(new MPlayerHealthChanged());
	}

	public function gainPlayerHealth(amt: Int) {
		this.state.currentHealth += amt;
		this.state.currentHealth = Math.clampI(this.state.currentHealth, 0, this.state.maxHealth);
		this.dispatcher.dispatch(new MPlayerHealthChanged());
	}

	public function moveCard(card: PlayerCard, location: PlayerCardLocation) {
		final source = card.location;
		final destination = location;
		if (source == destination) return;
		switch (source) {
			case Void:
			case Hand:
				this.state.inHand.remove(card);
			case Deck:
				this.state.inDeck.remove(card);
			case Playing:
			case Reward:
		}
		switch (destination) {
			case Void:
			case Hand:
				this.state.inHand.push(card);
			case Deck:
				this.state.inDeck.push(card);
			case Playing:
			case Reward:
		}
		card.location = location;
		this.dispatcher.dispatch(new MPlayerCardMoved(card, source, destination));
	}

	public function addStartingCards() {
		final startingCards = Globals.pCardFactory.getStartingCards(this);
		for (c in startingCards) {
			moveCard(c, Deck);
		}
		Utils.animatePop(this.nonBlockingAnimator, this.renderSystem.hudRenderSystem.iconDeck);
	}

	public function drawCards(amount: Int = 10) {
		while (this.state.inHand.length < 5 && this.state.inDeck.length != 0 && amount > 0) {
			final card = this.state.inDeck.randomItem(this.r);
			moveCard(card, Hand);
			amount -= 1;
		}
	}
}
