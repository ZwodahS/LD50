package world.systems;

import zf.ui.GenericButton;

class HudRenderSystem extends System {
	public var renderSystem: RenderSystem;
	public var drawLayer: h2d.Layers;

	public var iconHealth: h2d.Bitmap;
	public var textHealth: h2d.Text;
	public var iconDeck: h2d.Bitmap;
	public var textDeck: h2d.Text;

	public var titleLabel: h2d.Text;

	public var title(never, set): String;

	public var deckWindow: CardsWindow;

	public var quitButton: GenericButton;

	public function set_title(t: String): String {
		this.titleLabel.text = t;
		return t;
	}

	public function new(rs: RenderSystem, layer: h2d.Layers) {
		super();
		this.renderSystem = rs;
		this.drawLayer = layer;

		this.iconHealth = Globals.res.getBitmap("icon:health");
		this.iconDeck = Globals.res.getBitmap("icon:deck");
		this.textHealth = new h2d.Text(Assets.bodyFont4x);
		this.textHealth.text = '0';
		this.textHealth.dropShadow = {
			dx: 1,
			dy: 1,
			color: Colors.Black[0],
			alpha: 0.2
		};
		this.textHealth.textColor = Colors.White[0];
		this.textDeck = new h2d.Text(Assets.bodyFont4x);
		this.textDeck.textColor = Colors.White[0];
		this.textDeck.text = '20';
		this.textDeck.dropShadow = {
			dx: 1,
			dy: 1,
			color: Colors.Black[0],
			alpha: 0.2
		};

		this.iconHealth.y = 30;
		this.iconDeck.putBelow(iconHealth, [0, 10]);

		final alignX = 60;

		this.iconHealth.setX(alignX, AnchorRight, 5);
		this.iconDeck.setX(alignX, AnchorRight, 5);
		this.textHealth.centerYWithin(this.iconHealth).setX(alignX);
		this.textDeck.centerYWithin(this.iconDeck).setX(alignX);

		this.titleLabel = new h2d.Text(Assets.bodyFont4x);
		this.titleLabel.text = '';
		this.titleLabel.maxWidth = Globals.game.gameWidth;
		this.titleLabel.textAlign = Center;
		this.titleLabel.y = 4;

		this.drawLayer.add(iconHealth, 50);
		this.drawLayer.add(textHealth, 50);
		this.drawLayer.add(iconDeck, 50);
		this.drawLayer.add(textDeck, 50);
		this.drawLayer.add(titleLabel, 50);

		inline function makeButton(title: String): GenericButton {
			final button = new GenericButton(Assets.fromColor(Colors.ButtonBasic[0], Constants.ButtonSize.x + 20,
				Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[1], Constants.ButtonSize.x + 20, Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[2], Constants.ButtonSize.x + 20, Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[3], Constants.ButtonSize.x + 20, Constants.ButtonSize.y));
			button.font = Assets.bodyFont3x;
			button.text = title;
			return button;
		}
		this.quitButton = makeButton("QUIT");
		this.quitButton.setX(Globals.game.gameWidth, AnchorRight, 10).setY(10);
		this.quitButton.onLeftClick = () -> {
			Globals.game.switchScreen(new MenuScreen());
		}
		this.drawLayer.addChild(this.quitButton);
	}

	override public function reset() {
		super.reset();
		this.drawLayer.visible = false;
	}

	override public function init(world: World) {
		super.init(world);

		// @:listen RenderSystem MNewGameStarted 0
		dispatcher.listen(MNewGameStarted.MessageType, function(message: zf.Message) {
			showHud();
		}, 0);

		// @:listen HudRenderSystem MGameover 0
		dispatcher.listen(MGameover.MessageType, function(message: zf.Message) {
			onGameover();
		}, 0);

		// @:listen HudRenderSystem MPlayerHealthChanged 0
		dispatcher.listen(MPlayerHealthChanged.MessageType, function(message: zf.Message) {
			Utils.animatePop(this.world.nonBlockingAnimator, this.iconHealth);
		}, 0);

		// @:listen HudRenderSystem MSetTitle 0
		dispatcher.listen(MSetTitle.MessageType, function(message: zf.Message) {
			final m = cast(message, MSetTitle);
			this.title = m.title;
		}, 0);

		final bound = this.iconDeck.getBounds();
		final interactive = new h2d.Interactive(bound.width, bound.height, this.iconDeck);
		interactive.onOver = (e) -> {
			if (this.deckWindow != null) return;
			this.deckWindow = new CardsWindow(this.world.state.inDeck);
			this.drawLayer.addChild(this.deckWindow);
			this.renderSystem.windowRenderSystem.showWindow(this.deckWindow, this.iconDeck.getBounds(), {
				overrideSpacing: 10,
				preferredDirection: [Right],
			});
		}
		interactive.onOut = (e) -> {
			if (this.deckWindow == null) return;
			this.deckWindow.remove();
			this.deckWindow = null;
		}
		interactive.onWheel = function(e) {
			if (this.deckWindow == null) return;
			this.deckWindow.scroll(6 * e.wheelDelta);
		}
		interactive.cursor = Default;
	}

	override public function update(dt: Float) {
		if (this.world.state != null) {
			final state = this.world.state;
			this.textHealth.text = '${state.currentHealth}/${state.maxHealth}';
			this.textDeck.text = '${state.inDeck.length}';
		}
	}

	function showHud() {
		this.drawLayer.visible = true;
		this.drawLayer.alpha = 0;
		this.world.blockingAnimator.runAnim(new AlphaTo(this.drawLayer.wo(), 1, 1.0 / .5));
	}

	function onGameover() {
		// @todo animate
		this.drawLayer.visible = false;
	}
}
