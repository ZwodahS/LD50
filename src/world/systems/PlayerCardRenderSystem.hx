package world.systems;

class PlayerCardRenderSystem extends System {
	public var renderSystem: RenderSystem;
	public var drawLayer: h2d.Layers;

	public var inHand: Array<PlayerCard>;
	public var inReward: Array<PlayerCard>;

	public function new(rs: RenderSystem, layer: h2d.Layers) {
		super();
		this.renderSystem = rs;
		this.drawLayer = layer;
		this.inHand = [];
		this.inReward = [];
	}

	override public function reset() {
		for (c in this.inHand) {
			c.remove();
		}
		for (c in this.inReward) {
			c.remove();
		}
		this.inHand = [];
		this.inReward = [];
	}

	override public function init(world: World) {
		super.init(world);
		final dispatcher = world.dispatcher;

		// @:listen PlayerCardRenderingSystem MPlayerCardMoved 0
		dispatcher.listen(MPlayerCardMoved.MessageType, function(message: zf.Message) {
			onPlayerCardMoved(cast(message, MPlayerCardMoved));
		}, 0);

		// @:listen PlayerCardRenderSystem MGameover 0
		dispatcher.listen(MGameover.MessageType, function(message: zf.Message) {
			onGameover();
		}, 0);
	}

	function onPlayerCardMoved(m: MPlayerCardMoved) {
		hideCardTooltip();
		final iconDeck = this.world.renderSystem.hudRenderSystem.iconDeck;
		final iconBound = iconDeck.getBounds();
		m.card.targetPosition = null;
		// let's handle each case separately, then assert that the others wouldn't happen
		if (m.source == Deck && m.destination == Hand) { // handle card draw
			m.card.scaleX = 0.2;
			m.card.scaleY = 0.2;
			this.world.blockingAnimator.runAnim(new ScaleTo(m.card.wo(), [1.0, 1.0], 1.0 / .4), function() {
				m.card.scaleX = 1.0;
				m.card.scaleY = 1.0;
			});
			m.card.x = iconBound.x;
			m.card.y = iconBound.y;

			this.inHand.push(m.card);
			this.drawLayer.addChild(m.card);
		} else if (m.source == Void && m.destination == Deck) {
			// do nothing
		} else if (m.source == Void && m.destination == Reward) {
			m.card.scaleX = 0.2;
			m.card.scaleY = 0.2;
			m.card.alpha = 0;
			this.world.blockingAnimator.runAnims([
				new ScaleTo(m.card.wo(), [1.0, 1.0], 1.0 / .4),
				new AlphaTo(m.card.wo(), 1, 1.0 / .4),
			], function() {
				m.card.scaleX = 1.0;
				m.card.scaleY = 1.0;
				this.world.blockingAnimator.wait(1, () -> {});
			});
			this.inReward.push(m.card);
			this.drawLayer.addChild(m.card);
		} else if (m.source == Hand && m.destination == Void) {
			this.inHand.remove(m.card);
			this.world.blockingAnimator.runAnims([
				new AlphaTo(m.card.wo(), 0, 1.0 / .4),
				new MoveByAmountByDuration(m.card.wo(), [0, -30], .4),
			], () -> {
				m.card.remove();
			});
		} else if (m.source == Hand && m.destination == Deck) {
			this.inHand.remove(m.card);
			this.world.blockingAnimator.runAnims([
				new ScaleTo(m.card.wo(), [.2, .2], .8 / .4),
				new AlphaTo(m.card.wo(), 0, 1.0 / .4),
				new MoveToLocationByDuration(m.card.wo(), [iconBound.x, iconBound.y], .4),
			], function() {
				m.card.remove();
				m.card.scaleX = 1.0;
				m.card.scaleY = 1.0;
				m.card.alpha = 1.0;
			});
		} else if (m.source == Playing && m.destination == Void) {
			this.world.blockingAnimator.runAnims([
				new AlphaTo(m.card.wo(), 0, 1.0 / .4),
				new MoveByAmountByDuration(m.card.wo(), [0, -30], .4),
			], () -> {
				m.card.remove();
			});
		} else if (m.source == Playing && m.destination == Deck) {
			this.world.blockingAnimator.runAnims([
				new ScaleTo(m.card.wo(), [.2, .2], .8 / .4),
				new AlphaTo(m.card.wo(), 0, 1.0 / .4),
				new MoveToLocationByDuration(m.card.wo(), [iconBound.x, iconBound.y], .4),
			], function() {
				m.card.remove();
				m.card.scaleX = 1.0;
				m.card.scaleY = 1.0;
				m.card.alpha = 1.0;
			});
		} else if (m.source == Reward && m.destination == Deck) {
			this.inReward.remove(m.card);
			this.world.blockingAnimator.runAnims([
				new ScaleTo(m.card.wo(), [.2, .2], .8 / .4),
				new AlphaTo(m.card.wo(), 0, 1.0 / .4),
				new MoveToLocationByDuration(m.card.wo(), [iconBound.x, iconBound.y], .4),
			], function() {
				m.card.remove();
				m.card.scaleX = 1.0;
				m.card.scaleY = 1.0;
				m.card.alpha = 1.0;
			});
		} else if (m.source == Hand && m.destination == Playing) {
			this.drawLayer.add(m.card, 150);
			this.inHand.remove(m.card);
			final x = m.card.calculateXPosition(Globals.game.gameWidth, AlignCenter);
			this.world.blockingAnimator.runAnims([new MoveToLocationByDuration(m.card.wo(), [x, 150], .2),]);
		} else {
			Logger.debug('WARNING: ${m.source} -> ${m.destination}');
			// Assert.unreachable();
		}

		if (m.destination == Hand) {
			m.card.onOver = onCardOver;
			m.card.onOut = onCardOut;
		} else {
			m.card.onOver = (c, e) -> {};
			m.card.onOut = (c, e) -> {};
		}
		alignCards();
	}

	function alignCards() {
		var positions = calculateCardPositions(this.inHand);
		for (ind => pos in positions) {
			final c = this.inHand[ind];
			c.targetPosition = [pos, Globals.game.gameHeight - 130];
		}

		var positions = calculateCardPositions(this.inReward);
		for (ind => pos in positions) {
			final c = this.inReward[ind];
			c.x = pos;
			c.y = 80;
		}
	}

	override public function update(dt: Float) {
		for (c in this.inHand) {
			c.update(dt);
		}
		for (c in this.inReward) {
			c.update(dt);
		}
	}

	function calculateCardPositions(cards: Array<PlayerCard>): Array<Float> {
		var positions: Array<Float> = [];
		var totalWidth: Float = 0;
		final spacing = 10;
		for (ind in 0...cards.length) {
			final x = totalWidth + (spacing * ind);
			totalWidth += 80;
			positions.push(x);
		}
		totalWidth += (cards.length - 1) * spacing;
		final startX = (Globals.game.gameWidth - totalWidth) / 2;
		for (ind => c in positions) {
			positions[ind] = c + startX;
		}
		return positions;
	}

	function onGameover() {
		// @todo animate
		for (c in this.inHand) {
			c.remove();
		}
		for (c in this.inReward) {
			c.remove();
		}
		this.inHand = [];
		this.inReward = [];
	}

	var currentCardTooltip: CardTooltipWindow;

	function onCardOver(card: Card, event: hxd.Event) {
		if (Std.isOfType(card, PlayerCard) == false) return;
		card.y = Globals.game.gameHeight - 140;
		try {
			if (this.currentCardTooltip != null) this.currentCardTooltip.remove();
			this.currentCardTooltip = new CardTooltipWindow(cast(card, PlayerCard));
			this.renderSystem.windowRenderSystem.showWindow(this.currentCardTooltip, card.getBounds(), {
				preferredDirection: [Up],
				overrideSpacing: 10,
			});
		} catch (e) {
			Logger.exception(e);
		}
	}

	function onCardOut(card: Card, event: hxd.Event) {
		if (Std.isOfType(card, PlayerCard) == false) return;
		card.y = Globals.game.gameHeight - 130;
		hideCardTooltip();
	}

	function hideCardTooltip() {
		if (this.currentCardTooltip != null) this.currentCardTooltip.remove();
		this.currentCardTooltip = null;
	}
}
