package world.systems;

import zf.ui.GenericButton;

class ChoiceSystem extends System {
	var fightButton: GenericButton;
	var runButton: GenericButton;

	var restButton: GenericButton;
	var searchButton: GenericButton;
	var prepareButton: GenericButton;

	public var currentLevel: Level;

	public var currentLevelRO: h2d.Object;
	public var currentButtons: h2d.Object;

	public static final RunCost: Int = 2;

	public function new() {
		super();

		inline function makeButton(title: String): GenericButton {
			final button = new GenericButton(Assets.fromColor(Colors.ButtonBasic[0], Constants.ButtonSize.x,
				Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[1], Constants.ButtonSize.x, Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[2], Constants.ButtonSize.x, Constants.ButtonSize.y),
				Assets.fromColor(Colors.ButtonBasic[3], Constants.ButtonSize.x, Constants.ButtonSize.y));
			button.font = Assets.bodyFont3x;
			button.text = title;
			return button;
		}

		this.fightButton = makeButton("FIGHT");
		this.fightButton.onLeftClick = startFight;

		this.runButton = makeButton("RUN");
		this.runButton.onLeftClick = runFromFight;

		this.restButton = makeButton("REST");
		this.restButton.onLeftClick = restAction;

		this.searchButton = makeButton("SEARCH");
		this.searchButton.onLeftClick = searchAction;

		this.prepareButton = makeButton("PREPARE");
		this.prepareButton.onLeftClick = prepareAction;
	}

	override public function reset() {
		if (this.currentLevelRO != null) this.currentLevelRO.remove();
		if (this.currentButtons != null) this.currentButtons.remove();
		this.currentLevelRO = null;
		this.currentButtons = null;
		this.currentLevel = null;
	}

	override public function init(world: World) {
		super.init(world);
		// @:listen ChoiceSystem MShowNextChoice 100
		dispatcher.listen(MShowNextChoice.MessageType, function(message: zf.Message) {
			onShowNextChoice();
		}, 100);

		// @:listen ChoiceSystem MBattleCompleteEnemyDead 20
		dispatcher.listen(MBattleCompleteEnemyDead.MessageType, function(message: zf.Message) {
			onLevelEnd(true, false);
		}, 0);

		// @:listen ChoiceSystem MRunAway 0
		dispatcher.listen(MRunAway.MessageType, function(message: zf.Message) {
			onLevelEnd(false, true);
		}, 0);

		final window = new TooltipWindow(150, "Fight the enemy.");
		this.world.renderSystem.windowRenderSystem.attachWindowTooltip(this.fightButton, window,
			[Right, Left, Up, Down]);

		final window = new TooltipWindow(150, ['Destroy ${RunCost} cards and run away.',].join("\n"));
		this.world.renderSystem.windowRenderSystem.attachWindowTooltip(this.runButton, window, [Right, Left, Up, Down]);

		this.world.renderSystem.windowRenderSystem.attachWindowTooltip(this.restButton,
			new TooltipWindow(150, "Recover 10 health."), [Right, Left, Up, Down]);

		this.world.renderSystem.windowRenderSystem.attachWindowTooltip(this.searchButton,
			new TooltipWindow(150, "Gain 2 cards."), [Right, Left, Up, Down]);

		this.world.renderSystem.windowRenderSystem.attachWindowTooltip(this.prepareButton,
			new TooltipWindow(150, "Recover 1 usage to all cards in hand."), [Right, Left, Up, Down]);
	}

	function onShowNextChoice() {
		if (this.world.state == null) return;
		this.world.state.distance += 1;
		this.world.dispatcher.dispatch(new MSetTitle('DEPTH ${this.world.state.distance}'));
		try {
			final level = generateNextLevel();
			showLevel(level);
		} catch (e) {
			Logger.exception(e);
		}
	}

	function onLevelEnd(gainReward: Bool = false, showNextLevel: Bool = true) {
		if (gainReward) {
			if (this.currentLevel.hasReward()) {
				this.world.dispatcher.dispatch(new MSetTitle("REWARD"));
				this.world.blockingAnimator.wait(1, () -> {});
			}

			if (this.currentLevel.rewardCards > 0) {
				final cards: Array<PlayerCard> = [];
				for (_ in 0...this.currentLevel.rewardCards) {
					cards.push(Globals.pCardFactory.randomCard(this.world));
				}
				for (card in cards) {
					this.world.moveCard(card, Reward);
					this.world.blockingAnimator.wait(2, () -> {
						this.world.moveCard(card, Deck);
					});
				}
			}
			if (this.currentLevel.rewardHealth > 0) {
				this.world.gainPlayerHealth(this.currentLevel.rewardHealth);
			}
		}
		this.world.nonBlockingAnimator.waitFor(this.world.blockingAnimator.get_idle, () -> {
			this.currentLevel = null;
			onShowNextChoice();
			this.world.drawCards();
		});
	}

	function generateNextLevel(): Level {
		if (this.world.state.distance % 5 == 0) { // every 5 level gurantee a campfire
			final level = new Level(Rest);
			return level;
		} else {
			final enemy = Globals.enemyFactory.getEnemy(this.world.state.distance, this.world);
			final level = new Level(Battle, enemy);
			level.rewardCards = enemy.rewardCards;
			level.rewardHealth = enemy.rewardHealth;
			return level;
		}
	}

	function showLevel(level: Level) {
		this.currentLevel = level;
		var obj = new h2d.Object();
		obj.addChild(Globals.res.getBitmap("event:bg"));
		final bound = obj.getBounds();
		var title = "";
		final buttons: Array<zf.ui.Button> = [];
		switch (level.type) {
			case Rest:
				title = "CAMPFIRE";
				buttons.push(restButton);
				buttons.push(searchButton);
				buttons.push(prepareButton);
			case Battle:
				obj.addChild(level.enemy);
				level.enemy.x = 10;
				level.enemy.setY(bound.height, AnchorBottom, 10);
				title = "BATTLE";

				buttons.push(fightButton);
				buttons.push(runButton);

				final rewardTitle = new h2d.Text(Assets.bodyFont2x);
				rewardTitle.text = "REWARD";
				rewardTitle.textColor = Colors.Black[1];
				rewardTitle.textAlign = Center;
				rewardTitle.maxWidth = (bound.width / 2) - 20;
				rewardTitle.x = bound.width / 2 + 10;
				rewardTitle.y = 35;
				obj.addChild(rewardTitle);

				var prev: h2d.Object = rewardTitle;
				if (level.rewardCards > 0) {
					final icon = Globals.res.getBitmap("icon:deck");
					icon.putBelow(prev, [0, 5]);
					obj.addChild(icon);
					final count = new h2d.Text(Assets.bodyFont3x);
					count.textColor = Colors.Black[1];
					count.text = '${level.rewardCards}';
					count.putOnRight(icon, [5, 0]).centerYWithin(icon);
					obj.addChild(count);
					final window = new TooltipWindow(150, 'Gain ${level.rewardCards} Cards');
					this.world.renderSystem.windowRenderSystem.attachWindowTooltip(icon, window,
						[Right, Left, Up, Down]);
					prev = icon;
				}

				if (level.rewardHealth > 0) {
					final icon = Globals.res.getBitmap("icon:health");
					icon.putBelow(prev, [0, 5]);
					obj.addChild(icon);
					final count = new h2d.Text(Assets.bodyFont3x);
					count.textColor = Colors.Black[1];
					count.text = '${level.rewardHealth}';
					count.putOnRight(icon, [5, 0]).centerYWithin(icon);
					obj.addChild(count);
					final window = new TooltipWindow(150, 'Heal For ${level.rewardHealth}');
					this.world.renderSystem.windowRenderSystem.attachWindowTooltip(icon, window,
						[Right, Left, Up, Down]);
					prev = icon;
				}
		}

		final titleLabel = new h2d.Text(Assets.bodyFont3x);
		titleLabel.text = title;
		obj.addChild(titleLabel);
		titleLabel.textColor = Colors.Black[1];
		titleLabel.textAlign = Center;
		titleLabel.maxWidth = bound.width;
		titleLabel.y = 8;

		final buttonsLayer = new h2d.Flow();
		buttonsLayer.layout = Vertical;
		buttonsLayer.verticalSpacing = 5;
		buttonsLayer.horizontalAlign = Left;

		for (button in buttons) {
			buttonsLayer.addChild(button);
		}

		final anchor = Globals.game.gameWidth / 2 + 60;
		obj.setX(anchor, AnchorRight, 20).setY(40);
		buttonsLayer.setX(anchor, AnchorLeft, 10).setY(obj.y);
		this.world.renderSystem.choiceLayer.addChild(obj);
		this.world.renderSystem.choiceLayer.addChild(buttonsLayer);
		this.currentLevelRO = obj;
		this.currentButtons = buttonsLayer;
	}

	function startFight() {
		if (this.currentLevel.enemy == null) return;
		this.world.dispatcher.dispatch(new MStartFight(this.currentLevel.enemy));
		animateLevelHide();
	}

	function runFromFight() {
		final copy = [for (c in this.world.state.inHand) c];
		copy.shuffle(this.world.r);
		final discarded = copy.slice(0, RunCost);
		for (c in discarded) {
			this.world.moveCard(c, Void);
		}
		animateLevelHide();
		this.world.nonBlockingAnimator.waitFor(this.world.blockingAnimator.get_idle, () -> {
			this.world.blockingAnimator.wait(1, () -> {
				this.world.dispatcher.dispatch(new MRunAway());
			});
		});
	}

	function animateLevelHide(onFinish: Void->Void = null) {
		this.world.blockingAnimator.runAnim(new Batch([
			new AlphaTo(this.currentLevelRO.wo(), 0, 1.0 / .3),
			new AlphaTo(this.currentButtons.wo(), 0, 1.0 / .3),
		]), function() {
			this.currentLevelRO.remove();
			this.currentButtons.remove();
			if (onFinish != null) onFinish();
		});
	}

	function restAction() {
		animateLevelHide();
		this.world.gainPlayerHealth(10);
		this.world.nonBlockingAnimator.waitFor(this.world.blockingAnimator.get_idle, () -> {
			onLevelEnd(false, true);
		});
	}

	function searchAction() {
		animateLevelHide();
		this.world.dispatcher.dispatch(new MSetTitle("SEARCH RESULT"));
		final cards: Array<PlayerCard> = [];
		for (_ in 0...2) {
			cards.push(Globals.pCardFactory.randomCard(this.world));
		}
		for (card in cards) {
			this.world.moveCard(card, Reward);
			this.world.blockingAnimator.wait(2, () -> {
				this.world.moveCard(card, Deck);
			});
		}
		this.world.nonBlockingAnimator.waitFor(this.world.blockingAnimator.get_idle, () -> {
			onLevelEnd(false, true);
		});
	}

	function prepareAction() {
		animateLevelHide();
		this.world.dispatcher.dispatch(new MSetTitle("PREPARATION"));
		final animations: Array<Animation> = [];
		for (c in this.world.state.inHand) {
			animations.push(new MoveByAmountByDuration(c.wo(), [0, -30], .4));
		}
		this.world.blockingAnimator.runAnims(animations, () -> {
			for (c in this.world.state.inHand) {
				c.gainDurability(this.world.blockingAnimator);
			}
			this.world.nonBlockingAnimator.waitFor(this.world.blockingAnimator.get_idle, () -> {
				final animations: Array<Animation> = [];
				for (c in this.world.state.inHand) {
					animations.push(new MoveByAmountByDuration(c.wo(), [0, 30], .4));
				}
				this.world.blockingAnimator.runAnims(animations, () -> {
					onLevelEnd(false, true);
				});
			});
		});
	}
}
