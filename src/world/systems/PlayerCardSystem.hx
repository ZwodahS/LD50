package world.systems;

class PlayerCardSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: World) {
		super.init(world);
		final dispatcher = world.dispatcher;

		// @:listen PlayerCardSystem MPlayerCardMoved 0
		dispatcher.listen(MPlayerCardMoved.MessageType, function(message: zf.Message) {
			onPlayerCardMoved(cast(message, MPlayerCardMoved));
		}, 0);
	}

	function onPlayerCardMoved(m: MPlayerCardMoved) {
		if (m.destination == Hand) { // if we are moving to hand, then we register the handler
			m.card.onLeftClick = this.playCard;
		}
	}

	function playCard(card: Card, event: hxd.Event) {
		// only allow playing of cards when it is player's turn
		if (this.world.gameState != PlayerTurn) return;
		if (this.world.blockingAnimator.count != 0) return;

		final pCard = cast(card, PlayerCard);
		if (pCard.location != Hand) {
			Logger.warn('PLAYING CARD : ${pCard.cardName} FROM ${pCard.location}');
			return;
		}

		this.world.gameState = AnimatingCard;

		this.world.moveCard(cast(card, PlayerCard), Playing);
		this.world.nonBlockingAnimator.waitFor(function() {
			return this.world.blockingAnimator.count == 0;
		}, function() {
			applyCardEffect(cast(card, PlayerCard));
		});
	}

	public var currentCard: PlayerCard;
	public var effects: Array<CardEffect>;
	public var currentEffect: CardEffect;

	override public function update(dt: Float) {
		if (currentCard == null) return;
		if (currentEffect == null) {
			if (effects.length == 0) {
				onCardFinish();
			} else {
				currentEffect = effects.shift();
				currentEffect.applyEffect(this.world);
				this.world.nonBlockingAnimator.waitFor(this.world.blockingAnimator.get_idle, () -> {
					this.currentEffect = null;
				});
			}
		}
	}

	function applyCardEffect(card: PlayerCard) {
		this.currentCard = card;
		this.effects = [for (e in card.effects) e];
		card.depleteDurability(this.world.nonBlockingAnimator);
	}

	function onCardFinish() {
		if (this.currentCard.currentDurability <= 0) {
			this.world.moveCard(this.currentCard, Void);
		} else {
			this.world.moveCard(this.currentCard, Deck);
		}

		this.currentCard = null;
		this.effects = [];

		this.world.blockingAnimator.wait(0.5, () -> {});
		this.world.nonBlockingAnimator.waitFor(function() {
			return this.world.blockingAnimator.count == 0;
		}, function() {
			this.world.dispatcher.dispatch(new MCardPlayed(this.currentCard));
		});
	}
}
