package world.systems;

import zf.MessageDispatcher;

/**
	Define a generic system object.
**/
class System {
	public var world(default, null): World;
	public var dispatcher(default, null): MessageDispatcher;

	public function new() {}

	public function init(world: World) {
		this.world = world;
		this.dispatcher = world.dispatcher;
	}

	public function update(dt: Float) {}

	public function reset() {}
}
