package world.systems;

class RenderSystem extends System {
	public var drawLayer: h2d.Layers;

	public var choiceLayer: h2d.Layers;
	public var textLayer: h2d.Layers;

	public var hudLayer: h2d.Layers;
	public var cardLayer: h2d.Layers;

	public var hudRenderSystem: HudRenderSystem;
	public var playerCardRenderSystem: PlayerCardRenderSystem;
	public var windowRenderSystem: zf.ui.WindowRenderSystem;

	public function new() {
		super();
		this.drawLayer = new h2d.Layers();

		final windowBounds = new h2d.col.Bounds();
		windowBounds.xMin = 15;
		windowBounds.yMin = 15;
		windowBounds.xMax = Globals.game.gameWidth - 15;
		windowBounds.yMax = Globals.game.gameHeight - 15;
		this.windowRenderSystem = new zf.ui.WindowRenderSystem(windowBounds);

		this.drawLayer.add(this.choiceLayer = new h2d.Layers(), 50);
		this.drawLayer.add(this.textLayer = new h2d.Layers(), 60);
		this.drawLayer.add(this.cardLayer = new h2d.Layers(), 65);
		this.drawLayer.add(this.hudLayer = new h2d.Layers(), 80);
		this.drawLayer.add(this.windowRenderSystem.layer, 81);

		this.hudRenderSystem = new HudRenderSystem(this, this.hudLayer);
		this.playerCardRenderSystem = new PlayerCardRenderSystem(this, this.cardLayer);
	}

	override public function init(world: World) {
		super.init(world);

		// @:listen RenderSystem MShowTutorial 0
		dispatcher.listen(MShowTutorial.MessageType, function(message: zf.Message) {
			onShowTutorial();
		}, 0);

		// @:listen GameoverSystem MGameover 0
		dispatcher.listen(MGameover.MessageType, function(message: zf.Message) {
			final m = cast(message, MGameover);
			handleGameover(m);
		}, 0);

		world.addSystem(this.hudRenderSystem);
		world.addSystem(this.playerCardRenderSystem);
	}

	override public function update(dt: Float) {
		super.update(dt);
		this.windowRenderSystem.update(dt);
	}

	function onShowTutorial() {
		final text = new h2d.HtmlText(Assets.bodyFont2x);
		this.textLayer.addChild(text);
		text.text = StringConstants.formatHtmlString(StringConstants.TutorialString);
		text.maxWidth = Globals.game.gameWidth - 100;
		text.textAlign = Center;
		text.dropShadow = {
			dx: 1,
			dy: 1,
			color: Colors.Black[0],
			alpha: 0.2
		};
		text.x = 50;
		text.y = 20;

		final text2 = new h2d.HtmlText(Assets.bodyFont2x);
		this.textLayer.addChild(text2);
		text2.text = StringConstants.formatHtmlString(StringConstants.TutorialString2);
		text2.textAlign = Left;
		text2.dropShadow = {
			dx: 1,
			dy: 1,
			color: Colors.Black[0],
			alpha: 0.2
		};
		text2.putBelow(text).setX(220);

		final startGameButton = new zf.ui.GenericButton(Assets.fromColor(Colors.ButtonBasic[0],
			Constants.ButtonSize.x, Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[1], Constants.ButtonSize.x, Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[2], Constants.ButtonSize.x, Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[3], Constants.ButtonSize.x, Constants.ButtonSize.y));

		this.textLayer.addChild(startGameButton);
		startGameButton.font = Assets.bodyFont3x;
		startGameButton.text = "START";
		startGameButton.setX(Globals.game.gameWidth, AlignCenter);
		startGameButton.setY(Globals.game.gameHeight, AnchorBottom, 50);

		startGameButton.onLeftClick = startRun;
	}

	function handleGameover(m: MGameover) {
		final text = new h2d.HtmlText(Assets.bodyFont2x);
		this.textLayer.addChild(text);
		text.text = StringConstants.formatHtmlString(StringConstants.GameoverString[m.reason]);
		text.maxWidth = Globals.game.gameWidth - 100;
		text.textAlign = Center;
		text.dropShadow = {
			dx: 1,
			dy: 1,
			color: Colors.Black[0],
			alpha: 0.2
		};
		text.x = 50;
		text.y = 20;

		final text = new h2d.HtmlText(Assets.bodyFont4x);
		this.textLayer.addChild(text);
		text.text = [
			'DEPTH: ${this.world.state.distance}',
			'DRAGON SLAYED: ${this.world.state.dragonSlayed}',
		].join("<br/>");
		text.maxWidth = Globals.game.gameWidth - 100;
		text.textAlign = Center;
		text.dropShadow = {
			dx: 1,
			dy: 1,
			color: Colors.Black[0],
			alpha: 0.2
		};
		text.x = 50;
		text.y = 120;

		final startGameButton = new zf.ui.GenericButton(Assets.fromColor(Colors.ButtonBasic[0],
			Constants.ButtonSize.x, Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[1], Constants.ButtonSize.x, Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[2], Constants.ButtonSize.x, Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[3], Constants.ButtonSize.x, Constants.ButtonSize.y));

		this.textLayer.addChild(startGameButton);
		startGameButton.font = Assets.bodyFont3x;
		startGameButton.text = "RESTART";
		startGameButton.setX(Globals.game.gameWidth, AlignCenter);
		startGameButton.setY(Globals.game.gameHeight, AnchorBottom, 50);

		startGameButton.onLeftClick = startRun;
	}

	function startRun() {
		this.world.blockingAnimator.runAnim(new AlphaTo(this.textLayer.wo(), 0, 1.0 / .5), function() {
			this.textLayer.removeChildren();
			this.textLayer.alpha = 1.0;
			this.world.startRun();
		});
	}
}
