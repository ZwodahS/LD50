package world.systems;

class BattleSystem extends System {
	public var currentEnemy: EnemyCard;

	var endTurnButton: zf.ui.GenericButton;

	public function new() {
		super();
		// these buttons should technically be in another systems.

		this.endTurnButton = new zf.ui.GenericButton(Assets.fromColor(Colors.ButtonBasic[0], Constants.ButtonSize.x,
			Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[1], Constants.ButtonSize.x, Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[2], Constants.ButtonSize.x, Constants.ButtonSize.y),
			Assets.fromColor(Colors.ButtonBasic[3], Constants.ButtonSize.x, Constants.ButtonSize.y));
		this.endTurnButton.font = Assets.bodyFont3x;
		this.endTurnButton.text = "END TURN";
		this.endTurnButton.onLeftClick = endTurn;
	}

	override public function init(world: World) {
		super.init(world);

		// @:listen BattleSystem MStartFight 0
		dispatcher.listen(MStartFight.MessageType, function(message: zf.Message) {
			onStartFight(cast(message, MStartFight));
		}, 0);

		// @:listen BattleSystem MGameover 0
		dispatcher.listen(MGameover.MessageType, function(message: zf.Message) {
			this.endTurnButton.remove();
			onGameover();
		}, 0);

		// @:listen BattleSystem MCardPlayed 0
		dispatcher.listen(MCardPlayed.MessageType, function(message: zf.Message) {
			onCardPlayed(cast(message, MCardPlayed));
		}, 0);

		// @:listen BattleSystem MBattleCompleteEnemyDead 0
		dispatcher.listen(MBattleCompleteEnemyDead.MessageType, function(message: zf.Message) {
			final m = cast(message, MBattleCompleteEnemyDead);
			this.endTurnButton.remove();
			if (m.enemy != this.currentEnemy) return; // ????
			onEnemyDead(m);
		}, 0);
	}

	override public function reset() {
		if (this.currentEnemy != null) this.currentEnemy.remove();
		this.currentEnemy = null;
	}

	function onStartFight(m: MStartFight) {
		this.world.dispatcher.dispatch(new MSetTitle("BATTLE"));
		final bounds = m.enemy.getBounds();
		this.world.renderSystem.choiceLayer.addChild(m.enemy);
		m.enemy.x = bounds.x;
		m.enemy.y = bounds.y;
		this.currentEnemy = m.enemy;
		final destinationX = m.enemy.calculateXPosition(Globals.game.gameWidth, AlignCenter);
		final destinationY = 30;
		this.world.gameState = Animating;
		this.world.blockingAnimator.runAnim(new MoveToLocationByDuration(m.enemy.wo(), [destinationX, destinationY],
			.5), function() {
				this.world.gameState = PlayerTurn;
		});

		this.endTurnButton.visible = true;
		this.world.renderSystem.choiceLayer.addChild(this.endTurnButton);
		this.endTurnButton.setX(Globals.game.gameWidth, AnchorRight, 8);
		this.endTurnButton.setY(Globals.game.gameHeight, AnchorBottom, 8);
	}

	function onGameover() {
		// @todo animation
		if (this.currentEnemy != null) this.currentEnemy.remove();
		this.currentEnemy = null;
	}

	function onCardPlayed(m: MCardPlayed) {
		if (this.currentEnemy == null) return;
		if (checkBattleState() == true) {
			this.world.gameState = PlayerTurn;
		}
	}

	function onEnemyDead(m: MBattleCompleteEnemyDead) {
		if (currentEnemy.enemyName == "Dragon" || currentEnemy.enemyName == "Greater Dragon") {
			this.world.state.dragonSlayed += 1;
		}
		this.currentEnemy.remove();
		this.currentEnemy = null;
		this.world.gameState = EndOfBattle;
	}

	public function damageEnemy(source: h2d.Object, damage: Int, numHit: Int) {
		if (this.currentEnemy == null) return;
		damage = damage - this.currentEnemy.armor;
		damage = Math.clampI(damage, 0, null);
		var newHealth = this.currentEnemy.currentHealth - (damage * numHit);
		newHealth = Math.clampI(newHealth, 0, null);
		this.currentEnemy.currentHealth = newHealth;
		Utils.animatePop(this.world.blockingAnimator, this.currentEnemy.healthIcon);
	}

	public function endTurn() {
		this.endTurnButton.visible = false;
		if (this.currentEnemy == null) return;
		this.world.gameState = EnemyTurn;
		enemyAttack();
		this.world.nonBlockingAnimator.waitFor(this.world.blockingAnimator.get_idle, () -> {
			if (this.checkBattleState() == true) {
				startNewTurn();
			}
		});
	}

	function enemyAttack() {
		// @todo animate better
		final damage = this.currentEnemy.strength;
		this.world.damagePlayer(damage);
		Utils.animatePop(this.world.blockingAnimator, this.currentEnemy.strengthIcon);
	}

	function startNewTurn() {
		this.endTurnButton.visible = true;
		this.world.drawCards();
		this.world.gameState = PlayerTurn;
	}

	/**
		return true if the battle should continue, else means event have been dispatched
	**/
	function checkBattleState(): Bool {
		final state = this.world.state;
		if (state.currentHealth <= 0) {
			this.world.dispatcher.dispatch(new MGameover("health"));
			return false;
		} else if (state.inDeck.length == 0 && state.inHand.length == 0) {
			this.world.dispatcher.dispatch(new MGameover("cards"));
			return false;
		}
		if (currentEnemy.currentHealth <= 0) {
			this.world.dispatcher.dispatch(new MBattleCompleteEnemyDead(this.currentEnemy));
			return false;
		}
		return true;
	}
}
