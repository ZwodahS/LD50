import zf.ui.TileBoxFactory;

/**
	Assets is used to store loaded assets
**/
class Assets {
	public static var defaultFont: h2d.Font;

	public static var bodyFonts: Map<Int, h2d.Font>;
	public static var bodyFont0x: h2d.Font;
	public static var bodyFont1x: h2d.Font;
	public static var bodyFont2x: h2d.Font;
	public static var bodyFont3x: h2d.Font;
	public static var bodyFont4x: h2d.Font;

	public static var whiteBoxFactory: TileBoxFactory;

	public static function load() {
		Assets.bodyFonts = new Map<Int, h2d.Font>();
		for (s in [8, 10, 14, 20, 24, 32, 48]) {
			final font = hxd.Res.load('fonts/gluten_medium_${s}.fnt').to(hxd.res.BitmapFont).toFont();
			Assets.bodyFonts[s] = font;
			final resized = font.clone();
			resized.resizeTo(Std.int(s / 2));
			Assets.bodyFonts[Std.int(s / 2)] = resized;
		}
		Assets.bodyFont0x = Assets.bodyFonts[4];
		Assets.bodyFont1x = Assets.bodyFonts[5];
		Assets.bodyFont2x = Assets.bodyFonts[7];
		Assets.bodyFont3x = Assets.bodyFonts[10];
		Assets.bodyFont4x = Assets.bodyFonts[12];

		Assets.defaultFont = Assets.bodyFont1x;

		Globals.res = new zf.ResourceManager();
		Globals.res.addSpritesheet(zf.Assets.loadAseSpritesheetConfig('graphics/packed.json'));

		Assets.whiteBoxFactory = new TileBoxFactory(Globals.res.getTile("box:white"), 5, 5);
	}

	public static function fromColor(color: Color, width: Int, height: Int): h2d.Bitmap {
		final bm = new h2d.Bitmap(Globals.res.getTile("white"));
		bm.width = width;
		bm.height = height;
		bm.color.setColor(color);
		return bm;
	}
}
