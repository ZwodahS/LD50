class StringConstants {
	// @formatter:off
	public static var TutorialString: String = [
		"Welcome to The Pit.",
		"",
		"How deep can you go before you run out ?",
		"",
	].join("\n");

	public static final TutorialString2: String = [
		"- After you play a card, it will return to your deck.",
		"- The Deck is not ordered.",
		"- When you draw a card, a random one is drawn.",
		"- Cards stay in your hand until played.",
		"- Hand size limit is 5.",
		"- You will draw to full hand after every turn.",
		"- Every card has a limited number of uses.",
		"- After it is depleted. it is destroyed.",
		"- When you run out of cards, you lose.",
		"- Fight enemy to gain cards.",
		"- Lose cards if you choose to run.",
	].join("\n");

	public static var GameoverString: Map<String, String> = [
		'health' => ["You die.",].join("\n"),
		'cards' => ["You ran out of cards.",].join('\n'),
	];

	public static var HtmlMappings = ["\n" => "<br/>",];

	public static function formatHtmlString(str: String): String {
		for (value => target in HtmlMappings) {
			str = str.replace(value, target);
		}
		return str;
	}
}
