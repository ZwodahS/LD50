class GameScreen extends zf.Screen {
	var world: World;

	public function new() {
		super();
		this.world = new World();
		this.world.startNewGame();
		this.addChild(this.world.renderSystem.drawLayer);
	}

	override public function update(dt: Float) {
		this.world.update(dt);
	}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {}

	override public function destroy() {}
}
