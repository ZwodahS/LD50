package ui;

class CardTooltipWindow extends zf.ui.Window {
	public var card: PlayerCard;

	public function new(card: PlayerCard) {
		super();
		this.card = card;
	}

	override public function onShow() {
		this.removeChildren();

		var flow = new h2d.Flow();
		flow.layout = Vertical;
		flow.verticalSpacing = 5;

		for (effect in card.effects) {
			final keyword = effect.getKeyword();
			final tooltip = effect.getKeywordTooltip();
			if (keyword == null || tooltip == null) continue;

			final w = new h2d.Object();

			final title = new h2d.Text(Assets.bodyFont2x);
			title.text = keyword;
			title.textColor = Colors.Black[1];
			w.addChild(title);

			final description = new h2d.Text(Assets.bodyFont1x);
			description.maxWidth = 120;
			description.text = tooltip;
			description.textColor = Colors.Black[0];
			description.putBelow(title, [0, 4]);
			w.addChild(description);

			final bound = w.getBounds();
			final bg = Assets.whiteBoxFactory.make([130, Std.int(bound.height) + 6]);
			final obj = new h2d.Object();
			obj.addChild(bg);
			obj.addChild(w);
			w.x = 3;
			w.y = 3;
			flow.addChild(obj);
		}
		this.addChild(flow);
	}
}
