package ui;

class CardsWindow extends zf.ui.Window {
	var bg: h2d.Object;

	public var scrollArea: h2d.Object;
	public var mask: h2d.Mask;

	public function new(cards: Array<PlayerCard>) {
		super();
		final Size: Point2i = [400, 330];
		this.add(this.bg = Assets.whiteBoxFactory.make(Size), 0);

		this.scrollArea = new h2d.Object();

		cards = [ for (c in cards) c ];

		cards.sort((c1, c2) -> {
			if (c1.playerCardType != c2.playerCardType) {
				return c1.playerCardType - c2.playerCardType;
			}
			if (c1.cardName < c2.cardName) {
				return -1;
			} else if (c1.cardName > c2.cardName) {
				return 1;
			}
			return 0;
		});

		final startX = 5;
		final startY = 5;
		final NumPerRow = 4;
		for (ind => card in cards) {
			card.scaleX = 1.0;
			card.scaleY = 1.0;
			card.alpha = 1;
			card.visible = true;
			final x = startX + ((ind % NumPerRow) * 90);
			final y = startY + (Std.int(ind / NumPerRow) * 124);
			card.x = x;
			card.y = y;
			this.scrollArea.addChild(card);
		}

		this.mask = new h2d.Mask(Size.x, Size.y - 20);
		mask.addChild(scrollArea);
		mask.x = 10;
		mask.y = 10;

		var bound = new h2d.col.Bounds();
		bound.xMin = 0;
		bound.yMin = 0;
		bound.xMax = Size.x;
		bound.yMax = Math.max(Size.y - 20, scrollArea.getSize().height) + 10;
		mask.scrollBounds = bound;
		this.addChild(mask);
	}

	public function scroll(amt: Float) {
		mask.scrollY -= amt;
	}
}
