package ui;

class HealthIcon extends h2d.Object {
	var bg: h2d.Bitmap;
	var text: h2d.Text;

	public var textValue(get, set): String;

	public function new(value: String = '') {
		super();
		this.bg = Globals.res.getBitmap('icon:health');
		this.text = new h2d.Text(Assets.bodyFont3x);

		this.addChild(bg);
		final bounds = bg.getBounds();
		this.addChild(text);
		text.maxWidth = bounds.width;
		text.text = 'a';
		text.centerYWithin(bg);
		text.textAlign = Center;
		text.dropShadow = {
			dx: 1,
			dy: 1,
			color: Colors.Black[0],
			alpha: 0.5
		};
		this.textValue = value;
	}

	public function set_textValue(s: String): String {
		this.text.text = s;
		return this.text.text;
	}

	public function get_textValue(): String {
		return this.text.text;
	}
}
