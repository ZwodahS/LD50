package ui;

import zf.ui.TileBoxFactory;

class TooltipWindow extends zf.ui.Window {
	var text: h2d.HtmlText;
	var bg: h2d.Object;

	public var bgFactory: TileBoxFactory;

	public var tooltip: String;

	public function new(maxWidth: Int = 350, tooltip: String = null) {
		super();
		this.bgFactory = Assets.whiteBoxFactory;
		this.tooltip = tooltip;

		this.text = new h2d.HtmlText(Assets.bodyFont2x);
		this.text.text = getTooltip();
		this.text.textColor = Colors.Black[1];
		this.text.maxWidth = maxWidth;

		text.x = 5;
		text.y = 4;
		this.add(text, 1);
	}

	dynamic public function getTooltip(): String {
		return this.tooltip;
	}

	override public function onShow() {
		if (this.bg != null) this.bg.remove();
		var tooltipString = getTooltip();
		if (tooltipString == null) {
			this.visible = false;
			return;
		}
		tooltipString = StringConstants.formatHtmlString(tooltipString);
		this.visible = true;
		this.text.text = tooltipString.replace("\n", "<br />");
		final textBound = text.getSize();
		final size: Point2i = [Std.int(textBound.width) + 10, Std.int(textBound.height) + 11];
		this.add(this.bg = bgFactory.make(size), 0);
	}
}
