package ui;

class DurabilityIcon extends h2d.Object {
	var icon: h2d.Anim;

	public var depleted(default, set): Bool;

	public function set_depleted(b: Bool): Bool {
		this.depleted = b;
		this.icon.currentFrame = this.depleted ? 0 : 1;
		return this.depleted;
	}

	public function new(value: String = '') {
		super();
		this.icon = Globals.res.getAnim('card:durability');
		this.icon.pause = true;

		this.addChild(icon);
	}

	public function animateDeplete(animator: Animator) {
		final wo = this.icon.wo();
		wo.alignCenter();
		animator.runAnim(new Pop(wo, 0.4, .2), function() {
			this.depleted = true;
			wo.returnCenter();
		});
	}

	public function animateGain(animator: Animator) {
		final wo = this.icon.wo();
		wo.alignCenter();
		animator.runAnim(new Pop(wo, 0.4, .2), function() {
			this.depleted = false;
			wo.returnCenter();
		});
	}
}
