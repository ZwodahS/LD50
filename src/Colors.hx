class Colors {
	// All colors to be defined as [Light, Normal, Dark]
	public static final Black: Array<Color> = [0xff393836, 0xff111012, 0];
	public static final White: Array<Color> = [0xfffffbe5, 0xfffff3c4, 0];

	// buttons color for the 4 different state [default, hover, disabled, selected]
	public static final ButtonBasic: Array<Color> = [0xff615f59, 0xff88867c, 0xff88867c, 0xffb2a372];

	public static final Background: Color = 0xFF203037;
}
